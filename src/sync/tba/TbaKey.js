const store = require('store');

export const getKey = () => {
  let tbaKey = store.get('settings/tba/key');
  if (typeof tbaKey === 'undefined') {
    tbaKey = 'gw1O9gQFSTwA34JTXENXtrlLhGR1ktwSLNdyxYcdOCeKLx8RvzBGTc52E5Ci2cZa';
    store.set('settings/tba/key', tbaKey);
  }
  return tbaKey;
}
